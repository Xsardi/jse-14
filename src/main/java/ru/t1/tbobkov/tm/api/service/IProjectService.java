package ru.t1.tbobkov.tm.api.service;

import ru.t1.tbobkov.tm.api.repository.IProjectRepository;
import ru.t1.tbobkov.tm.enumerated.Sort;
import ru.t1.tbobkov.tm.enumerated.Status;
import ru.t1.tbobkov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IProjectRepository {

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    List<Project> findAll(Sort sort);

}
