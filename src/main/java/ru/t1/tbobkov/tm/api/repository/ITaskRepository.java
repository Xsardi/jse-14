package ru.t1.tbobkov.tm.api.repository;

import ru.t1.tbobkov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> findAllByProjectId(String projectId);

    Task add(Task task);

    void clear();

    Task create(String name, String description);

    Task create(String name);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    int getSize();

}