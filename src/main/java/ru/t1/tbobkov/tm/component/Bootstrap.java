package ru.t1.tbobkov.tm.component;

import ru.t1.tbobkov.tm.api.controller.ICommandController;
import ru.t1.tbobkov.tm.api.controller.IProjectController;
import ru.t1.tbobkov.tm.api.controller.IProjectTaskController;
import ru.t1.tbobkov.tm.api.controller.ITaskController;
import ru.t1.tbobkov.tm.api.repository.ICommandRepository;
import ru.t1.tbobkov.tm.api.service.ICommandService;
import ru.t1.tbobkov.tm.api.repository.IProjectRepository;
import ru.t1.tbobkov.tm.api.service.IProjectService;
import ru.t1.tbobkov.tm.api.repository.ITaskRepository;
import ru.t1.tbobkov.tm.api.service.IProjectTaskService;
import ru.t1.tbobkov.tm.api.service.ITaskService;
import ru.t1.tbobkov.tm.constant.ArgumentConstant;
import ru.t1.tbobkov.tm.constant.CommandConstant;
import ru.t1.tbobkov.tm.controller.CommandController;
import ru.t1.tbobkov.tm.controller.ProjectController;
import ru.t1.tbobkov.tm.controller.ProjectTaskController;
import ru.t1.tbobkov.tm.controller.TaskController;
import ru.t1.tbobkov.tm.enumerated.Status;
import ru.t1.tbobkov.tm.model.Project;
import ru.t1.tbobkov.tm.model.Task;
import ru.t1.tbobkov.tm.repository.CommandRepository;
import ru.t1.tbobkov.tm.repository.ProjectRepository;
import ru.t1.tbobkov.tm.repository.TaskRepository;
import ru.t1.tbobkov.tm.service.CommandService;
import ru.t1.tbobkov.tm.service.ProjectService;
import ru.t1.tbobkov.tm.service.ProjectTaskService;
import ru.t1.tbobkov.tm.service.TaskService;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService, projectService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private void initDemoData() {
        projectService.add(new Project("A_TEST_PROJ_1_PROGRESS", Status.IN_PROGRESS));
        projectService.add(new Project("C_TEST_PROJ_2_NOTSTARTED", Status.NOT_STARTED));
        projectService.add(new Project("B_TEST_PROJ_3_COMPLETED", Status.COMPLETED));
        projectService.add(new Project("D_TEST_PROJ_4_NOTSTARTED", Status.NOT_STARTED));

        taskService.add(new Task("C_TEST_TASK_1_COMPLETED", Status.COMPLETED));
        taskService.add(new Task("B_TEST_TASK_2_NOTSTARTED", Status.NOT_STARTED));
        taskService.add(new Task("D_TEST_TASK_3_NOTSTARTED", Status.NOT_STARTED));
        taskService.add(new Task("A_TEST_TASK_4_INPROGRESS", Status.IN_PROGRESS));
    }

    private void processArgs(final String[] args) {
        if (args == null || args.length < 1) return;
        processArg(args[0]);
        exit();
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            System.out.printf("\n");
            System.out.println("ENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    // @formatter:off
    private void processArg(final String arg) {
        switch (arg.toLowerCase()) {
            case ArgumentConstant.VERSION: commandController.showVersion(); break;
            case ArgumentConstant.ABOUT: commandController.showAbout(); break;
            case ArgumentConstant.HELP:  commandController.showHelp(); break;
            case ArgumentConstant.INFO: commandController.showSystemInfo(); break;
            default: commandController.showArgumentError(); break;
        }
    }

    private void processCommand(final String command) {
        switch (command.toLowerCase()) {
            case CommandConstant.VERSION: commandController.showVersion(); break;
            case CommandConstant.ABOUT: commandController.showAbout(); break;
            case CommandConstant.HELP: commandController.showHelp(); break;
            case CommandConstant.INFO: commandController.showSystemInfo(); break;
            case CommandConstant.PROJECT_CREATE: projectController.createProject(); break;
            case CommandConstant.PROJECT_LIST: projectController.showProjects(); break;
            case CommandConstant.PROJECT_CLEAR: projectController.clearProjects(); break;
            case CommandConstant.PROJECT_UPDATE_BY_INDEX: projectController.updateProjectByIndex(); break;
            case CommandConstant.PROJECT_UPDATE_BY_ID: projectController.updateProjectById(); break;
            case CommandConstant.PROJECT_REMOVE_BY_INDEX: projectController.removeProjectByIndex(); break;
            case CommandConstant.PROJECT_REMOVE_BY_ID: projectController.removeProjectById(); break;
            case CommandConstant.PROJECT_SHOW_BY_INDEX: projectController.showProjectByIndex();  break;
            case CommandConstant.PROJECT_SHOW_BY_ID: projectController.showProjectById(); break;
            case CommandConstant.PROJECT_CHANGE_STATUS_BY_INDEX: projectController.changeProjectStatusByIndex(); break;
            case CommandConstant.PROJECT_CHANGE_STATUS_BY_ID: projectController.changeProjectStatusById(); break;
            case CommandConstant.PROJECT_START_BY_INDEX: projectController.startProjectByIndex(); break;
            case CommandConstant.PROJECT_START_BY_ID: projectController.startProjectById(); break;
            case CommandConstant.PROJECT_COMPLETE_BY_INDEX: projectController.completeProjectByIndex(); break;
            case CommandConstant.PROJECT_COMPLETE_BY_ID: projectController.completeProjectById(); break;
            case CommandConstant.TASK_CREATE: taskController.createTask(); break;
            case CommandConstant.TASK_LIST: taskController.showTasks(); break;
            case CommandConstant.TASK_CLEAR: taskController.clearTasks(); break;
            case CommandConstant.TASK_UPDATE_BY_INDEX: taskController.updateTaskByIndex(); break;
            case CommandConstant.TASK_UPDATE_BY_ID: taskController.updateTaskById(); break;
            case CommandConstant.TASK_REMOVE_BY_INDEX: taskController.removeTaskByIndex(); break;
            case CommandConstant.TASK_REMOVE_BY_ID: taskController.removeTaskById(); break;
            case CommandConstant.TASK_SHOW_BY_INDEX: taskController.showTaskByIndex(); break;
            case CommandConstant.TASK_SHOW_BY_ID: taskController.showTaskById(); break;
            case CommandConstant.TASK_CHANGE_STATUS_BY_INDEX: taskController.changeTaskStatusByIndex(); break;
            case CommandConstant.TASK_CHANGE_STATUS_BY_ID: taskController.changeTaskStatusById(); break;
            case CommandConstant.TASK_START_BY_INDEX: taskController.startTaskByIndex(); break;
            case CommandConstant.TASK_START_BY_ID: taskController.startTaskById(); break;
            case CommandConstant.TASK_COMPLETE_BY_INDEX: taskController.completeTaskByIndex(); break;
            case CommandConstant.TASK_COMPLETE_BY_ID: taskController.completeTaskById(); break;
            case CommandConstant.TASK_BIND_TO_PROJECT: projectTaskController.bindTaskToProject(); break;
            case CommandConstant.TASK_UNBIND_FROM_PROJECT: projectTaskController.unbindTaskFromProject(); break;
            case CommandConstant.TASK_LIST_BY_PROJECT_ID: taskController.showTasksByProjectId(); break;
            case CommandConstant.EXIT: exit(); break;
            default: commandController.showCommandError(); break;
        }
    }
    // @formatter:on

    private void exit() {
        System.exit(0);
    }

    public void run(final String... args) {
        initDemoData();
        commandController.showWelcome();
        processArgs(args);
        processCommands();
    }

}